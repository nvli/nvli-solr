# nvli-solr
Solr for E-News and Govt Website Crawl 

How to run
-------------
1. download nvli-solr.zip to /opt
2. Extract the folder to /opt
3. Move to nvli-solr
4. To start solr, enter the command ./bin/solr start -p 7983
